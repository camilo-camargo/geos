; tty_string(string);
tty_string: 
	push bp 
	mov bp, sp 

	push ax
	push bx
	push cx
	mov ah, 0x0E  
	mov bx, [bp+4]
	xor cx, cx

tty_string_loop:
	mov al, [bx]

	cmp al, 0
	je tty_string_done

	inc bx
	int 0x10
	jmp tty_string_loop
	

tty_string_done:
	pop cx 
	pop bx
	pop ax

	pop bp
	ret


