tty_hex:
	;--- c calling convention
	push bp 
	mov bp, sp   
	;--- register used in this function push
	push ax
	push bx
	push cx
	push dx
	mov bx, [bp+4]  ;var
	mov cx, 4 


tty_hex_body:         
	;--- function body
	mov dx, bx       ; temp=var 
	and bx, 0xf000   ; and (var, 0xf000)    
	rol bh, 4
	mov al, bh  

 	cmp  al, 9 
	jle tty_hex_num 
	jg tty_hex_char   

tty_hex_num:
	;print 0-to-9
	add al, '0'  
	jmp tty_hex_body2
tty_hex_char:
	;print a-to-f
	sub al, 10
	add al, 'a'  
	jmp tty_hex_body2 

tty_hex_body2:
	;--- printing with bios and shift left
	mov ah, 0x0E
	int 0x10  
	mov bx, dx      ; var = temp
	shl bx, 4
	loop tty_hex_body 

tty_hex_done:    
	;--- register used in this function popped
	pop dx
	pop cx
	pop bx
	pop ax

	;--- c calling convention
	pop bp
	ret 


