; tty_cls(color)

tty_cls:    
	push bp
	mov bp, sp

	push ax
	push bx
	push cx
	push dx 

	mov ah, 0x06   ;scroll up (down 0x07) 
	mov bh, byte [bp+4]   ;blank area attributes
	mov al, 0      ; 0 means clear windows 
	mov ch, 0      ; y top
	mov cl, 0 		; x top
	mov dh, 0xff ; y bottom
	mov dl, 0xff ; x bottom
	int 0x10 

	mov ah, 0x02
	xor bx, 0
	xor dx, 0
	int 0x10

	pop dx 
	pop cx
	pop dx
	pop ax 

	pop bp
	ret 
