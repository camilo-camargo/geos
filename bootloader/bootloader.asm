[bits 16] 
[org 0x7C00]  ;Bios update the bootloader at 0x7C00    
KERNEL_OFFSET equ 0x1000

;bios initial values to registers 
;dl -> disk driver  

;real mode memory-map        
;0x100000: Bios
;0xC0000 : Video Memory
;0xA0000 : Extended Bios Data Area
;0x9FC00 :Free
;0x7E00  :Boot Sector end
;0x7C00  :Boot Sector start
;0x500   :
;0x400   :BIOS Data Area
;0x0     :IVT 
mov bp, 0x9fc0
mov sp, bp

mov dh, 0
mov [BOOT_DRIVE], dx



vesa_mode:
	push ax
	push bx

	mov ax, 0x4F02 
	mov bx, 0x010C
	int 0x10 
	
	pop ax
	pop bx

start: 
	push  0xf0
	call tty_cls
	pop ax  

	push bp
	call tty_hex
	pop bp 

	push KERNEL_OFFSET
	call disk_read
	pop bx

switching_rm_to_pm: 
	cli
	lgdt [gdt_descriptor]
	
	mov eax, cr0
	or eax, 0x1
	mov cr0,eax

	call CODE_SEG:protected_mode
	call KERNEL_OFFSET
	jmp $

credits:
	push GEOS_BOOT_MSG 
	call tty_string
	pop ax

	push GEOS_BOOT_MSG_DEV
	call tty_string
	pop ax

	cli
	hlt
	jmp $

%include "tty/tty_string.asm"
%include "tty/tty_cls.asm"
%include "tty/tty_hex.asm"
%include "disk/disk.asm" 
%include "gdt.asm"

BOOT_DRIVE: db 0
GEOS_BOOT_MSG: db "3DOS OPERATING SYSTEM ", 0
GEOS_BOOT_MSG_DEV: db "Develop by: Camilo Camargo", 0
DISK_ERROR_MSG: db "Error reading the disk", 0 

[bits 32]
protected_mode:
	mov ax, DATA_SEG	 
	mov ds, ax
	mov ss, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	mov ebp, 0x90000
	mov esp, ebp
	ret

times 510 - ($ - $$) db 0 ;Bios searched at the first sector of the drive 
dw 0xAA55                 ;Magic key (BIOS)
