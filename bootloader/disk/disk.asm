;disk_read(buffer)
disk_read:
	push bp 
	mov bp, sp
	push ax
	push bx
	push cx
	push dx	

	xor ax, ax
	mov dl, [BOOT_DRIVE]
	int 0x13
	jc disk_read_error

	mov ah, 0x02         ;read function
	mov al, 1            ;Sectors to read
	mov ch, 0            ;Cylinder 
	mov cl, 2	         ;Sector
	mov dh, 0            ;Head
	mov dl, [BOOT_DRIVE] ; Drive
	mov bx, [bp+4]
	int 0x13  

	jc disk_read_error
	
	pop dx	
	pop cx	
	pop bx	
	pop ax	

	pop bp 
	ret

disk_read_error:
	push 	DISK_ERROR_MSG
	call tty_string
	pop ax
	ret 


