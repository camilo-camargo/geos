BOOTLOADER_FILES=$(wildcard bootloader/*.asm bootloader/**/*.asm )
BOOTLOADER=bootloader/bootloader.asm
BOOTLOADER_PATH=bootloader/ 

KERNEL_PATH=kernel/
KERNEL_ENTRY=kernel/kernel_entry.asm
KERNEL_FILES=$(wildcard $(KERNEL_PATH)/*.c $(KERNEL_PATH)/**/*.c)



compile: 
	nasm -f bin -i$(BOOTLOADER_PATH)  $(BOOTLOADER)
	nasm -f elf32 -i$(KERNEL_PATH) $(KERNEL_ENTRY)
	$(foreach obj,$(KERNEL_FILES) ,gcc -c -m32 -fno-pie -ffreestanding $(obj) -o $(obj:.c=.o);)
	ld -m elf_i386 -Ttext 0x1000 $(KERNEL_ENTRY:.asm=.o) $(KERNEL_FILES:.c=.o) --oformat binary -entry main -o $(KERNEL_PATH)kernel
	dd if=/dev/zero of=hdd bs=512 count=$(shell echo 1024*2*10 | bc)
	dd if=$(BOOTLOADER:.asm=) of=hdd bs=512  conv=notrunc
	dd if=$(KERNEL_PATH)kernel of=hdd bs=512  seek=1 conv=notrunc
	qemu-system-x86_64 hdd
