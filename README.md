# GEOS 
Graphics Engine Operating System
Operating system for i386 architecture. 
 
Features
- [X] Native Bootloader
- [X] Multi Stage Bootloader
- [X] Switching to Protected Mode
- [X] Kernel mixing with C programming Language 
- [ ] Add FAT 12 file system (Bootloader) 
- [ ] VESA Implementation
- [ ] Video Controller  
